package main

import (
    "fmt"
    "log"
)

func handler(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Hello, this is xiaomage who focus on Cloud Native DevSecOps field.")
}

func main() {
    http.HandleFunc("/devopsday", handler)
    log.Fatal(http.ListenAndServe(":9999", nil))
}
